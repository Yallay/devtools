#!/bin/bash
if [ ! -f jdk-11.0.1_linux-x64_bin.deb ];then
  wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/11.0.1+13/90cf5d8f270a4347a95050320eef3fb7/jdk-11.0.1_linux-x64_bin.deb
# faut dl le jdk en tar.gz pur eclipse
# tomcat requiere un jre pas un jdk
fi
sudo dpkg -i jdk-11.0.1_linux-x64_bin.deb
# ci dessous valable pour jre
echo -e 'export JAVA_HOME="usr/lib/jvm/jdk-11.0.1"' >> ~/.bashrc
echo -e 'export PATH=$PATH:$JAVA_HOME/bin' >> ~/.bashrc
source ~/.bashrc
